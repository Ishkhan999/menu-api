from django.db import models
from Orders.models import Order
from Vehicles.models import Vehicle
from Products.models import Product


class OrderDetail(models.Model):
    OrderID = models.ForeignKey(Order, on_delete=models.CASCADE)
    ProductID = models.ForeignKey(Product, on_delete=models.CASCADE)
    Quantity = models.IntegerField()
