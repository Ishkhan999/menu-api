from django.db import models

# Create your models here.
class Category(models.Model):
    CategoryName = models.CharField(max_length=200)
    Description = models.TextField()
    Photo = models.CharField(max_length=200)