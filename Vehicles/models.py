from django.db import models


class Vehicle(models.Model):
    Maker =  models.CharField(max_length=200)
    Model = models.CharField(max_length=200)
    Year = models.CharField(max_length=200)
    Color = models.CharField(max_length=200)
    Category = models.CharField(max_length=200)
    Number_Plate = models.CharField(max_length=200)
   
