from django.db import models



class Customer(models.Model):
    CustomerName = models.CharField(max_length=200)
    Country = models.CharField(max_length=200)
    City = models.CharField(max_length=200)
    Phone = models.IntegerField()
    Rating = models.CharField(max_length=200)
    Bonus = models.CharField(max_length=200)

