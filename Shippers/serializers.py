from .models import Shipper
from rest_framework import serializers


class ShipperCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipper
        fields = '__all__'


class ShipperListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipper
        fields = '__all__'



class ShipperDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipper
        fields = '__all__'

