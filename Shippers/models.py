from django.db import models
from Vehicles.models import Vehicle

class Shipper(models.Model):
    FirstName = models.CharField(max_length=200)
    SurName = models.CharField(max_length=200)
    Country = models.CharField(max_length=200)
    City = models.CharField(max_length=200)
    Address = models.CharField(max_length=200)
    PassportID = models.CharField(max_length=200)
    Phone = models.CharField(max_length=200)
    VehicleID = models.ForeignKey(Vehicle,on_delete=models.CASCADE)
    Rating = models.CharField(max_length=200)
    Bonus = models.CharField(max_length=200)