from .views import ShipperCreateAPIView, ShipperListAPIView,ShipperDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', ShipperCreateAPIView.as_view()),
    path('all', ShipperListAPIView.as_view()),
    path('detail/<int:pk>', ShipperDetailAPIView.as_view()),
    

]
