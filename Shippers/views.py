from .models import Shipper
from .serializers import ShipperCreateSerializer,ShipperListSerializer,ShipperDetailSerializer
from rest_framework import generics


class ShipperCreateAPIView(generics.CreateAPIView):
    serializer_class = ShipperCreateSerializer
    queryset = Shipper.objects.all()

class ShipperListAPIView(generics.CreateAPIView):
    serializer_class = ShipperListSerializer
    queryset = Shipper.objects.all()

class ShipperDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ShipperDetailSerializer
    queryset = Shipper.objects.all()    