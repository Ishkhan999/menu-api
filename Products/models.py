from django.db import models
from Categories.models import Category
from Suppliers.models import Supplier



class Product(models.Model):
    ProductName = models.CharField(max_length= 200)
    SupplierID = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    CategoryID = models.ForeignKey(Category, on_delete=models.CASCADE)
    Unit = models.CharField(max_length= 200)
    Price = models.CharField(max_length= 200)
        