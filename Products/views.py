from .models import Product
from .serializers import ProductCreateSerializer,ProductListSerializer,ProductDetailSerializer
from rest_framework import generics


class ProductCreateAPIView(generics.CreateAPIView):
    serializer_class = ProductCreateSerializer
    queryset = Product.objects.all()



class ProductListAPIView(generics.CreateAPIView):
    serializer_class = ProductListSerializer
    queryset = Product.objects.all()    

class ProductDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ProductDetailSerializer
    queryset = Product.objects.all()    
