from .views import ProductCreateAPIView,ProductListAPIView,ProductDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', ProductCreateAPIView.as_view()),
    path('all', ProductListAPIView.as_view()),
    path('detail/<int:pk>', ProductListAPIView.as_view()),
    

]
