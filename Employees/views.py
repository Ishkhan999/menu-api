from .models import Employee
from .serializers import EmployeeCreateSerializer,EmployeeListSerializer,EmployeeDetailSerializer
from rest_framework import generics


class EmployeeCreateAPIView(generics.CreateAPIView):
    serializer_class = EmployeeCreateSerializer
    queryset = Employee.objects.all()

class EmployeeListAPIView(generics.ListAPIView):
    serializer_class = EmployeeListSerializer
    queryset = Employee.objects.all()    


class EmployeeDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = EmployeeListSerializer
    queryset = Employee.objects.all()        
