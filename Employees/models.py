from django.db import models

class Employee(models.Model):
    FirstName = models.CharField(max_length=200)
    SurName = models.CharField(max_length=200)
    PassportID = models.CharField(max_length=200)
    Birthday = models.CharField(max_length=200)
    Photo = models.CharField(max_length=200)
    Notes = models.CharField(max_length=200)
    Bonus = models.CharField(max_length=200)
