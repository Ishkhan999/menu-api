from .views import EmployeeCreateAPIView,EmployeeListAPIView,EmployeeDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', EmployeeCreateAPIView.as_view()),
    path('all', EmployeeListAPIView.as_view()),
    path('detail/<int:pk>', EmployeeDetailAPIView.as_view()),
    

]
