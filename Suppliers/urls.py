from .views import SupplierCreateAPIView,SupplierListAPIView,SupplierDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', SupplierCreateAPIView.as_view()),
    path('all', SupplierListAPIView.as_view()),
    path('detail/<int:pk>', SupplierListAPIView.as_view()),
    

]
