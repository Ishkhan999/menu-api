from .models import Supplier
from rest_framework import serializers


class SupplierCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Supplier
        fields = '__all__'


class SupplierListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Supplier
        fields = '__all__'        



class SupplierDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Supplier
        fields = '__all__'        