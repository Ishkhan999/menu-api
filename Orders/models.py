from django.db import models
from Customers.models import Customer
from Employees.models import Employee
from Shippers.models import Shipper



class Order(models.Model):
    CustomerID = models.ForeignKey(Customer, on_delete=models.CASCADE)
    EmployeeID = models.ForeignKey(Employee, on_delete=models.CASCADE)
    OrderDatetime = models.DateTimeField(auto_now_add = True)
    ShipperID = models.ForeignKey(Shipper, on_delete=models.CASCADE)
    
        