from django.apps import AppConfig


class OrdersandshippersConfig(AppConfig):
    name = 'OrdersAndShippers'
