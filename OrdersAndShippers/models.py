from django.db import models
from Orders.models import Order
from Shippers.models import Shipper



class OrderAndShipper(models.Model):
    OrderID = models.ForeignKey(Order, on_delete=models.CASCADE)
    ShipperID = models.ForeignKey(Shipper, on_delete=models.CASCADE)
    
        