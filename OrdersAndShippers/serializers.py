from .models import OrderAndShipper
from rest_framework import serializers


class OrderAndShipperCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderAndShipper
        fields = '__all__'


class OrderAndShipperListSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderAndShipper
        fields = '__all__'



class OrderAndShipperDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderAndShipper
        fields = '__all__'




