from .views import OrderAndShipperCreateAPIView,OrderAndShipperListAPIView,OrderAndShipperDetailAPIView
from django.urls import path

urlpatterns = [

    path('create',OrderAndShipperCreateAPIView.as_view()),
    path('all', OrderAndShipperListAPIView.as_view()),
    path('detail/<int:pk>', OrderAndShipperDetailAPIView.as_view()),
    

]
