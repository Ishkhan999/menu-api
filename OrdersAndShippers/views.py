from .models import OrderAndShipper
from .serializers import OrderAndShipperCreateSerializer,OrderAndShipperListSerializer,OrderAndShipperDetailSerializer
from rest_framework import generics


class OrderAndShipperCreateAPIView(generics.CreateAPIView):
    serializer_class = OrderAndShipperCreateSerializer
    queryset = OrderAndShipper.objects.all()



class OrderAndShipperListAPIView(generics.CreateAPIView):
    serializer_class = OrderAndShipperListSerializer
    queryset = OrderAndShipper.objects.all()    

class OrderAndShipperDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = OrderAndShipperDetailSerializer
    queryset = OrderAndShipper.objects.all()    
